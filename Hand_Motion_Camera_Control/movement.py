from enum import Enum

class movement(Enum):
    ROTATE_RIGHT = 1
    FORWARD = 2
    ROTATE_LEFT = 3

    RIGHT = 4
    STAY = 5
    LEFT = 6

    UP = 7
    BACK = 8
    DOWN = 9