import threading
from time import sleep

import cv2 as cv
import imutils
import numpy as np
import movement
import queue

class HandMotionCameraControl:
    def __init__(self):
        self.frameWidth = self.frameHeight = 900
        self.min_YCrCb = np.array([0, 133, 77], np.uint8)
        self.max_YCrCb = np.array([255, 173, 127], np.uint8)
        self.DIRECTION = movement.movement.STAY

    def getDirection(self):
        return self.DIRECTION

    def setDirection(self, dir):
        self.DIRECTION = dir

    # ========== WARN_USER_IF_HAND_NOT_RECOGNISED ==========
    def warnHandMissing(self, frame):
        if not self.isHandRecognizes:
            cv.putText(frame, "NO HAND RECOGNISED", (int(self.frameWidth / 20), int(self.frameHeight / 15)),
                       cv.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2)

    def positionOfHand(self, xy):
        if 0 <= xy[0] < int(self.frameWidth / 3) and 0 <= xy[1] < int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.ROTATE_RIGHT:
                self.DIRECTION = movement.movement.ROTATE_RIGHT
        if int(self.frameWidth / 3) <= xy[0] < 2 * int(self.frameWidth / 3) and 0 <= xy[1] < int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.FORWARD:
                self.DIRECTION = movement.movement.FORWARD
        if 2 * int(self.frameWidth / 3) <= xy[0] < int(self.frameWidth) and 0 <= xy[1] < int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.ROTATE_LEFT:
                self.DIRECTION = movement.movement.ROTATE_LEFT

        if 0 <= xy[0] < int(self.frameWidth / 3) and int(self.frameHeight / 4) <= xy[1] < 2 * int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.RIGHT:
                self.DIRECTION = movement.movement.RIGHT
        if int(self.frameWidth / 3) <= xy[0] < 2 * int(self.frameWidth / 3) and int(self.frameHeight / 4) <= xy[
            1] < 2 * int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.STAY:
                self.DIRECTION = movement.movement.STAY
        if 2 * int(self.frameWidth / 3) <= xy[0] < int(self.frameWidth) and int(self.frameHeight / 4) <= xy[
            1] < 2 * int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.LEFT:
                self.DIRECTION = movement.movement.LEFT

        if 0 <= xy[0] < int(self.frameWidth / 3) and 2 * int(self.frameHeight / 4) <= xy[1] < 3 * int(
                self.frameHeight / 4):
            if self.DIRECTION != movement.movement.UP:
                self.DIRECTION = movement.movement.UP
        if int(self.frameWidth / 3) <= xy[0] < 2 * int(self.frameWidth / 3) and 2 * int(self.frameHeight / 4) <= xy[
            1] < 3 * int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.BACK:
                self.DIRECTION = movement.movement.BACK
        if 2 * int(self.frameWidth / 3) <= xy[0] < int(self.frameWidth) and 2 * int(self.frameHeight / 4) <= xy[
            1] < 3 * int(self.frameHeight / 4):
            if self.DIRECTION != movement.movement.DOWN:
                self.DIRECTION = movement.movement.DOWN


    # ========== USING_CAMERA ==========
    def start(self):
        cap = cv.VideoCapture(0)
        while (cap.isOpened()):
            ret, frame = cap.read()
            frame = imutils.resize(frame, width=self.frameWidth, height=self.frameHeight)

            # MAKEING THE LINES (HORIZONTAL)
            cv.line(frame, (0, int(self.frameHeight / 4)), (self.frameWidth, int(self.frameHeight / 4)), (0, 255, 0),
                    2)  # X, Y, COLOR, THICKNESS
            cv.line(frame, (0, 2 * int(self.frameHeight / 4)), (self.frameWidth, 2 * int(self.frameHeight / 4)),
                    (0, 255, 0), 2)  # X, Y, COLOR, THICKNESS
            # MAKEING THE LINES (VERTICAL)
            cv.line(frame, (int(self.frameWidth / 3), 0), (int(self.frameWidth / 3), self.frameHeight), (0, 255, 0),
                    2)  # X, Y, COLOR, THICKNESS
            cv.line(frame, (2 * int(self.frameWidth / 3), 0), (2 * int(self.frameWidth / 3), self.frameHeight),
                    (0, 255, 0), 2)  # X, Y, COLOR, THICKNESS

            # ========== LOOKING_FOR_HAND ==========
            # HAND_MASK
            imageYCrCb = cv.cvtColor(frame, cv.COLOR_BGR2YCR_CB)

            # Find region with skin tone in YCrCb image
            skinRegion = cv.inRange(imageYCrCb, self.min_YCrCb, self.max_YCrCb)

            # Do contour detection on skin region
            contours, hierarchy = cv.findContours(skinRegion, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

            try:
                contours = max(contours, key=lambda x: cv.contourArea(x))
                area = cv.contourArea(contours)
                if area > 15000:
                    cv.drawContours(frame, [contours], -1, (255, 255, 0), 2)
                    x, y, w, h = cv.boundingRect(contours)
                    cv.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    center = (x, y)
                    self.positionOfHand(center)
                cv.imshow("contours", frame)
            except Exception as e:
                print("Need hand to be seen")

            # cv.imshow('frame', frame)
            if cv.waitKey(1) & 0xFF == ord('q'):
                break

        # When everything done, release the capture
        cap.release()
        cv.destroyAllWindows()
