import pygame as pygame

from djitellopy import tello
import cv2
import HandMotionCameraControl as handMotion
from threading import Thread
import queue
import movement
import keyboard
import time

class droneControll:

    def __init__(self):
        self.q = queue.Queue()
        self.drone = tello.Tello()
        self.speed = 40
        self.drone.set_speed(self.speed)
        self.control = handMotion.HandMotionCameraControl()
        print("Constructor done")

    def start(self):
        Thread(target=self.run).start()
        Thread(target=self.control.start(), args=(self.control,)).start()

    def run(self):
        self.drone.connect()
        print(self.drone.get_battery())
        if self.drone.streamon():
            print("ready to stream")
        self.drone.takeoff()
        while True:
            img = self.drone.get_frame_read().frame


            cv2.imshow("image", img)

            print(self.control.getDirection())
            self.movement()
            time.sleep(0.05)
            cv2.waitKey(1)

    def movement(self):
        if keyboard.is_pressed('l'):
            self.drone.land()
        if keyboard.is_pressed('t'):
            self.drone.takeoff()

        if self.control.getDirection() == movement.movement.ROTATE_LEFT:
            self.drone.send_rc_control(0,0,0,-self.speed)
        if self.control.getDirection() == movement.movement.FORWARD:
            self.drone.send_rc_control(0, self.speed, 0, 0)
        if self.control.getDirection() == movement.movement.ROTATE_RIGHT:
            self.drone.send_rc_control(0,0,0,self.speed)

        if self.control.getDirection() == movement.movement.LEFT:
            self.drone.send_rc_control(-self.speed,0,0,0)
        if self.control.getDirection() == movement.movement.STAY:
            print("staying in position")
        if self.control.getDirection() == movement.movement.RIGHT:
            self.drone.send_rc_control(self.speed,0,0,0)

        if self.control.getDirection() == movement.movement.UP:
            self.drone.move_up(self.speed)
            self.drone.send_rc_control(0, 0, self.speed, 0)
        if self.control.getDirection() == movement.movement.BACK:
            self.drone.send_rc_control(0,-self.speed, 0, 0)
        if self.control.getDirection() == movement.movement.DOWN:
            self.drone.send_rc_control(0, 0, -self.speed, 0)

        # self.drone.send_rc_control()



drone = droneControll()
drone.start()
